// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBDT4Nl8vsicSHzQDhCeH16S2w-M3tzR6A",
    authDomain: "about-me-5af21.firebaseapp.com",
    databaseURL: "https://about-me-5af21.firebaseio.com",
    projectId: "about-me-5af21",
    storageBucket: "about-me-5af21.appspot.com",
    messagingSenderId: "985405076007"
  },
  socketServer: 'http://localhost:3000'
};
