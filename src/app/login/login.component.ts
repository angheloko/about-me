import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: Observable<firebase.User>;
  isLoggedIn = false;
  userLoaded = false;

  constructor(public afDb: AngularFireDatabase, public afAuth: AngularFireAuth, public router: Router) {
    this.user = afAuth.authState;

    this.user.subscribe(auth => {
      this.userLoaded = true;

      if (auth) {
        console.log(auth);
        this.afDb.object('/users/' + auth.uid).set({
          name: auth.displayName
        });
        this.isLoggedIn = true;
        this.router.navigate(['/client']);
      }
      else {
        this.isLoggedIn = false;
      }
    });
  }

  ngOnInit() {
  }

  login() {
    this.userLoaded = false;
    this.afAuth.auth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
  }

  logout() {
    this.afAuth.auth.signOut();
  }

}
