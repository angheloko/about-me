export const QUESTIONS = [{
  id: 'philosophy',
  question: 'What is your philosphy in life?',
  choices: [{
    value: 'A',
    label: 'Freedom is what you do with what\'s been done to you.'
  }, {
    value: 'B',
    label: 'Complaining is silly. Either act or forget.'
  }, {
    value: 'C',
    label: 'Happiness only real when shared.'
  }, {
    value: 'D',
    label: 'Don\'t die young only to be burried when you\'re old'
  }],
  answer: 'C'
}, {
  id: 'fave-self',
  question: 'What is your favorite thing about yourself?',
  choices: [{
    value: 'A',
    label: 'My physical appearance'
  }, {
    value: 'B',
    label: 'How smart I am'
  }, {
    value: 'C',
    label: 'My persistence'
  }, {
    value: 'D',
    label: 'None'
  }],
  answer: 'C'
}, {
  id: 'change-self',
  question: 'What\'s the one thing you would like to change about yourself?',
  choices: [{
    value: 'A',
    label: 'Physical appearance'
  }, {
    value: 'B',
    label: 'Sleeping hours'
  }, {
    value: 'C',
    label: 'Passiveness'
  }, {
    value: 'D',
    label: 'None. I\'m satisfied with I am.'
  }],
  answer: 'C'
}, {
  id: 'religious-spiritual',
  question: 'Are you religious or spiritual?',
  choices: [{
    value: 'A',
    label: 'Religious'
  }, {
    value: 'B',
    label: 'Spiritual'
  }, {
    value: 'C',
    label: 'Both'
  }, {
    value: 'D',
    label: 'Neither'
  }],
  answer: 'D'
}, {
  id: 'growing-up',
  question: 'Is what you\'re doing now what you always wanted to do growing up?',
  choices: [{
    value: 'A',
    label: 'Yes'
  }, {
    value: 'B',
    label: 'No'
  }],
  answer: 'B'
}, {
  id: 'book-movie',
  question: 'What\'s your favorite book/movie of all time and why did it speak to you so much?',
  choices: [{
    value: 'A',
    label: 'Gattaca'
  }, {
    value: 'B',
    label: 'A Scanner Darkly'
  }, {
    value: 'C',
    label: 'The Positronic Man'
  }, {
    value: 'D',
    label: 'Inception'
  }, {
    value: 'E',
    label: 'Falling Down'
  }, {
    value: 'F',
    label: 'Man from Earth'
  }],
  answer: 'A'
}, {
  id: 'talk-person',
  question: 'Who is that one person you can talk to about just anything?',
  choices: [{
    value: 'A',
    label: 'Mother / Father'
  }, {
    value: 'B',
    label: 'Best friend'
  }, {
    value: 'C',
    label: 'Brother'
  }, {
    value: 'D',
    label: 'Partner'
  }],
  answer: 'B'
}, {
  id: 'ideal-weekend',
  question: 'What\'s an ideal weekend for you?',
  choices: [{
    value: 'A',
    label: 'Laptop + couch'
  }, {
    value: 'B',
    label: 'Partner + movie + couch'
  }, {
    value: 'C',
    label: 'Friends + alcohol + couch'
  }, {
    value: 'D',
    label: 'Outdoors!'
  }],
  answer: 'A'
}, {
  id: 'confrontational',
  question: 'Are you confrontational?',
  choices: [{
    value: 'A',
    label: 'Yes'
  }, {
    value: 'B',
    label: 'No'
  }],
  answer: 'B'
}, {
  id: 'misunderstood',
  question: 'What\'s the one thing that people always misunderstand about you?',
  choices: [{
    value: 'A',
    label: 'That I always know what I\'m doing'
  }, {
    value: 'B',
    label: 'That I enjoy what I do'
  }, {
    value: 'C',
    label: 'That I\'m not busy'
  }, {
    value: 'D',
    label: 'That I don\'t get mad'
  }],
  answer: 'D'
}, {
  id: 'ideal-vacation',
  question: 'What is your idea of a perfect vacation?',
  choices: [{
    value: 'A',
    label: 'Europe countryside (i.e. Tuscanny to Provence)'
  }, {
    value: 'B',
    label: 'Relaxing in a beach in the Philippines'
  }, {
    value: 'C',
    label: 'Exploring the busy streets of Asia'
  }, {
    value: 'D',
    label: 'Luxury cruise'
  }],
  answer: 'B'
}, {
  id: 'think-often',
  question: 'What do you think about most often?',
  choices: [{
    value: 'A',
    label: 'My games'
  }, {
    value: 'B',
    label: 'Going home'
  }, {
    value: 'C',
    label: 'Bugs'
  }, {
    value: 'D',
    label: 'What other people think of me'
  }],
  answer: 'B'
}, {
  id: 'keeps-up-night',
  question: 'What keeps you up at night?',
  choices: [{
    value: 'A',
    label: 'Bugs'
  }, {
    value: 'B',
    label: 'Hopes and ambitions'
  }, {
    value: 'C',
    label: 'That next great idea'
  }, {
    value: 'D',
    label: 'Scary thoughts'
  }],
  answer: 'A'
}, {
  id: 'spirit-animal',
  question: 'What would be your spirit animal?',
  choices: [{
    value: 'A',
    label: 'Eagle'
  }, {
    value: 'B',
    label: 'Honeybadger'
  }, {
    value: 'C',
    label: 'Lion'
  }, {
    value: 'D',
    label: 'Sloth'
  }],
  answer: 'D'
}];
