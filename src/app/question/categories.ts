export const CATEGORIES = [{
  id: 'biography',
  label: 'Biography'
}, {
  id: 'love',
  label: 'Love'
}, {
  id: 'career',
  label: 'Career'
}, {
  id: 'personal',
  label: 'Personal'
}];
