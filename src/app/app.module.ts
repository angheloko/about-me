import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from '@angular/material';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';
import { QuestionComponent } from './question/question.component';
import { AdminComponent } from './admin/admin.component';
import { ClientComponent } from './client/client.component';

import { environment } from '../environments/environment';
import { LoginComponent } from './login/login.component';

const ROUTES: Routes = [{
  path: 'questions',
  component: QuestionComponent
}, {
  path: 'admin',
  component: AdminComponent
}, {
  path: 'login',
  component: LoginComponent
}, {
  path: 'client',
  component: ClientComponent
}];

@NgModule({
  declarations: [
    AppComponent,
    QuestionComponent,
    AdminComponent,
    ClientComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(ROUTES),
    BrowserAnimationsModule,
    MaterialModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
